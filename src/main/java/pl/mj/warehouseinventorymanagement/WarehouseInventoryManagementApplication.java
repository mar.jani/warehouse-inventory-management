package pl.mj.warehouseinventorymanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WarehouseInventoryManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(WarehouseInventoryManagementApplication.class, args);
	}

}
